class InsufficientException(Exception):
    pass

class MobileInventory:
    def __init__(self,inventory):
        if inventory == None or {}:
            self.balance_inventory = {}
        else:
            MobileInventory.check_data_type(inventory,'Input inventory')
            self.balance_inventory = inventory

    def add_stock(self,new_stock):
        MobileInventory.check_data_type(new_stock,'Input stock')
        for ip_op_key in new_stock.keys():
            if self.check_if_key_exist(ip_op_key):
                self.balance_inventory[ip_op_key] = self.balance_inventory[ip_op_key] + new_stock[ip_op_key]
            else:
                self.balance_inventory.update({ip_op_key:new_stock[ip_op_key]})

    def sell_stock(self,requested_stock):
        MobileInventory.check_data_type(requested_stock,'Requested stock')
        for ip_op_key in requested_stock.keys():
            if self.check_if_key_exist(ip_op_key) == False:
                raise InsufficientException('No Stock. New Model Request')
        for ip_op_key in requested_stock.keys():
            if self.check_if_value_sufficient(ip_op_key,requested_stock):
                self.balance_inventory[ip_op_key] = self.balance_inventory[ip_op_key] - requested_stock[ip_op_key]
            else:
                raise InsufficientException('Insufficient Stock')

    @staticmethod
    def check_data_type(data,error_msg):
        if isinstance(data,dict) == False:
            raise TypeError("{} must be a dictionary".format(error_msg))
        elif set(map(type,data.keys())) != {str}:
            raise ValueError('Mobile model name must be a string')
        elif ( set(map(type,data.values())) != {int}) or ( set(v > 0 for v in data.values()) != {True}):
            raise ValueError('No. of mobiles must be a positive integer')
        else:
            pass

    def check_if_key_exist(self,key):
        if self.balance_inventory.get(key) == None:
            return False
        else:
            return True

    def check_if_value_sufficient(self,key,ip):
        if self.balance_inventory[key] >= ip[key]:
            return True
        else:
            return False
        
    
#c = MobileInventory({'iPhone Model X':2, 'Xiaomi Model Y': 2, 'Nokia Model Z':2})
#print(c.balance_inventory)
#c.add_stock({'iPhone Model X':2, 'Oneplus Model X': 2})
#print(c.balance_inventory)
#c.sell_stock({'Oneplus Model X': 2, 'Nokia Model Z':2})
#print(c.balance_inventory)
#c.sell_stock({'iPhone Model X': 2, 'Karbon Model A':2})
#print(c.balance_inventory)


